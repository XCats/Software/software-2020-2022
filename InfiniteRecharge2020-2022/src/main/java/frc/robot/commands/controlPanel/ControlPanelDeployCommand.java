package frc.robot.commands.controlPanel;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ControlPanelSubsystem;

public class ControlPanelDeployCommand extends CommandBase {
    @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
    private final ControlPanelSubsystem mControlPanelSubsystem;
    private boolean mIsFinished;

    public ControlPanelDeployCommand(ControlPanelSubsystem controlPanelSubsystem) {
        super();
        mControlPanelSubsystem = controlPanelSubsystem;
        addRequirements(mControlPanelSubsystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        mControlPanelSubsystem.retract();
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        mControlPanelSubsystem.stopPanel();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }

}