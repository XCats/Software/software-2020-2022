package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.turret.TurretPIDCommand;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.CarouselSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.TurretSubsystem;

public class TurnToTargetAndShootCommand extends SequentialCommandGroup {

    private final Limelight mLimelight;

    public TurnToTargetAndShootCommand(TurretSubsystem turretSubsystem, Limelight limelight, 
        ShooterSubsystem shooterSubsystem, CarouselSubsystem carouselSubsystem) {
        mLimelight = limelight;    
        addCommands(/*new TurnOnLimelightLEDsCommand(limelight),*/ new TurretPIDCommand(turretSubsystem, limelight), 
            new ShootAndRunCarouselCommand(shooterSubsystem, limelight, carouselSubsystem));
    }

    @Override
    public void initialize(){
        System.out.println("TurnToTargetAndShootCommand -- Initialize");
        super.initialize();
    }

    @Override
    public void end(boolean interrupted) {
        System.out.println("TurnToTargetAndShootCommand -- End");
        // mpk new TurnOffLimelightLEDsCommand(mLimelight);
        super.end(interrupted);
    }
}