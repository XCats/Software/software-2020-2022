package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ShooterSubsystem;

public class SpinUpShooterCommand extends CommandBase {

    private final ShooterSubsystem mShooterSubsystem;

    public SpinUpShooterCommand(ShooterSubsystem shooterSubSystem) {
        mShooterSubsystem = shooterSubSystem;
        super.addRequirements(shooterSubSystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        System.out.println("Spin up shooter has been initialized");
    }


    @Override
    public void execute() {
        System.out.println("executing spin up shooter");
        mShooterSubsystem.setRPM(Constants.SHOOTER_SPINUP_RPM.getValue());
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        mShooterSubsystem.stopShooter();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return isFinished();
    }
}