package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import frc.robot.commands.carousel.CarouselCommand;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.CarouselSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

public class ShootAndRunCarouselCommand extends ParallelRaceGroup {

    public ShootAndRunCarouselCommand(ShooterSubsystem shooterSubsystem, Limelight limelight, CarouselSubsystem carouselSubsystem) {
        addCommands(new ShooterCommand(shooterSubsystem, limelight), 
            new CarouselCommand(carouselSubsystem, CarouselSubsystem.Mode.Shoot));
    }

}