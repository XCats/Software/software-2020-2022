package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.ShooterSubsystem;

public class ShooterCommand extends CommandBase {

    private final ShooterSubsystem mShooterSubsystem;
    private boolean mIsFinished;
    private final Limelight mLimelight;

    public ShooterCommand(ShooterSubsystem shooterSubSystem, Limelight limelight) {
        mLimelight = limelight;
        mShooterSubsystem = shooterSubSystem;
        super.addRequirements(shooterSubSystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        System.out.println("Shooter Command -- initialized");
    }

    @Override
    public void execute() {
        double rpm = Constants.DEFAULT_SHOOTER_RPM.getValue();
        // if(mShooterSubsystem.getNumPowerCells() == 0) {
        if(mLimelight.getDistance() < 1E300) rpm = Constants.SHOOTER_TREE_MAP.getInterpolated(mLimelight.getDistance());

        // mShooterSubsystem.setRPM(Constants.DEFAULT_SHOOTER_RPM.getValue());
        mShooterSubsystem.setRPM(rpm);
        if(mShooterSubsystem.isAtVelocitySetpoint()) {
            mShooterSubsystem.setFeederSpeed(Constants.DEFAULT_FEEDER_MOTOR_SPEED.getValue());
        } else {
            mShooterSubsystem.setFeederSpeed(0);
        }
        //     mIsFinished = true;
        // }
        //mShooterSubsystem.setFeederSpeed(Constants.DEFAULT_FEEDER_MOTOR_SPEED.getValue());
        // mShooterSubsystem.setFeederSpeed(Constants.DEFAULT_FEEDER_MOTOR_SPEED.getValue()); // mpk
        //System.out.println("Feed wheel speed: " + Constants.DEFAULT_FEEDER_MOTOR_SPEED);
    
        // mShooterSubsystem.setShooterSpeed(.3);
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        System.out.println("Shooter Command -- end");
        mShooterSubsystem.stopFeeder();
        mShooterSubsystem.stopShooter();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }
}