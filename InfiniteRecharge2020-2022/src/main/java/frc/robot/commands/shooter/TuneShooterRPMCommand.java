package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.utils.PropertyManager;
import frc.robot.utils.PropertyManager.IProperty;

public class TuneShooterRPMCommand extends CommandBase {

    private final ShooterSubsystem mShooterSubsystem;
    private boolean mIsFinished;
    private IProperty<Integer> mShooterRPM = new PropertyManager.IntProperty("ShooterRpm", 3000);
    private int infrequentPrint = 0;

    public TuneShooterRPMCommand(ShooterSubsystem shooterSubSystem) {
        mShooterSubsystem = shooterSubSystem;
        super.addRequirements(shooterSubSystem);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        System.out.println("Shooter Tune RPM has been initialized");
    }


    @Override
    public void execute() {
        double rpm = Constants.DEFAULT_SHOOTER_RPM.getValue();

        mShooterSubsystem.setRPM(rpm);
        if(mShooterSubsystem.isAtVelocitySetpoint()) {
            mShooterSubsystem.setFeederSpeed(Constants.DEFAULT_FEEDER_MOTOR_SPEED.getValue());
        } else {
            mShooterSubsystem.setFeederSpeed(0);
        }

    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        mShooterSubsystem.stopFeeder();
        mShooterSubsystem.stopShooter();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }
}