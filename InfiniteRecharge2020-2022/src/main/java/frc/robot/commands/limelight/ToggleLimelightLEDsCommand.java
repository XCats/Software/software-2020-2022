package frc.robot.commands.limelight;

/*----------------------------------------------------------------------------*/

/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

import frc.robot.processors.Limelight;

import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.ConditionalCommand;

/**
 * An example command that uses an example subsystem.
 */
public class ToggleLimelightLEDsCommand extends ConditionalCommand {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  //private final Limelight mLimelight;
  private boolean mIsFinished = false;
  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.4
   */
  public ToggleLimelightLEDsCommand(Limelight limelight) {
    super(new TurnOffLimelightLEDsCommand(limelight), 
      new TurnOnLimelightLEDsCommand(limelight), limelight::isLEDOn);

      // new BooleanSupplier() {
    
      //   @Override
      //   public boolean getAsBoolean() {
      //     return limelight.getLEDstate() == Limelight.LEDState.ON;
      //   }
      // }

  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    super.initialize();
    System.out.println("Toggle LED Command -- Initialize");
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
    System.out.println("Toggle LED Command -- End");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return mIsFinished;
  }
}
