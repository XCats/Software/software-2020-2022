package frc.robot.commands.turret;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.TurretSubsystem;
import frc.robot.utils.XboxButtonMap;

public class TurretDefaultCommand extends CommandBase {

  TurretSubsystem mTurret;
  XboxController mOperatorController;

  public TurretDefaultCommand (TurretSubsystem turret, XboxController operatorController) {
      super();
      this.mTurret = turret;
      this.mOperatorController = operatorController; 
    
      this.addRequirements(mTurret);
  }
     // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double speed = mOperatorController.getRawAxis(XboxButtonMap.LEFT_X_AXIS);
    if (Math.abs(speed) > .2) {
    //  mTurret.setSpeed(speed);  // Can return to this if limit switches are working better
      if (speed > 0) speed = 0.2; else speed = -0.2;
      // Moving joystick right will move turret CCW, making target appear to "move" right
      // Moving joystick left will move turret CW, making target appear to "move" left
      mTurret.setSpeed(speed); // right ->CCW ; left -> CW
    } else {
      mTurret.setSpeed(0);
    }
    SmartDashboard.putNumber("Turret joystick value", speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}