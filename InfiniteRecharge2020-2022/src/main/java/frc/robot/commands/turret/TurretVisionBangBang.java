package frc.robot.commands.turret;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.TurretSubsystem;

public class TurretVisionBangBang extends CommandBase {

  private TurretSubsystem mTurret;
  private Limelight mLimelight;

  public TurretVisionBangBang(TurretSubsystem turret, Limelight limelight) {
    this.mTurret = turret;
    this.mLimelight = limelight;

    this.addRequirements(mTurret, mLimelight);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("Running turret bang bang");
    if (mLimelight.seesTarget()) {
      mTurret.bangBang(mLimelight.getRawYawError());
    }

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    boolean isFinished = false;
    if (Math.abs(mLimelight.getRawYawError()) < Constants.TURRET_ALLOWABLE_ERROR.getValue()) {
      isFinished = true;
    }

    return isFinished;
  }
}