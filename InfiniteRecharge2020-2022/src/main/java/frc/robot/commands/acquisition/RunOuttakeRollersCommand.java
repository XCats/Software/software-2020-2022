/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.acquisition;

import frc.robot.subsystems.AcquisitionRollersSubsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * An example command that uses an example subsystem.
 */
public class RunOuttakeRollersCommand extends CommandBase {
  @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })
  private final AcquisitionRollersSubsystem mAcquisitionRollersSubsystem;

  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public RunOuttakeRollersCommand(AcquisitionRollersSubsystem acquisitionRollersSubsystem) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(acquisitionRollersSubsystem);
    mAcquisitionRollersSubsystem = acquisitionRollersSubsystem; 
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("RunIntakeRollersCommand -- Initialized");
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("RunIntakeRollersCommand -- Executed");
    mAcquisitionRollersSubsystem.outTake();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    System.out.println("RunIntakeRollersCommand -- Ended");
    mAcquisitionRollersSubsystem.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
