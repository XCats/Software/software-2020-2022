/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.acquisition;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.AcquisitionArmSubsystem;
import frc.robot.subsystems.AcquisitionRollersSubsystem;
import frc.robot.subsystems.CarouselSubsystem;

/**
 * An example command that uses an example subsystem.
 */
public class ArmDownThenIntakeCommand extends SequentialCommandGroup {
  @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })
  private  final AcquisitionArmSubsystem mAquisitionArmSubsystem;
  private final AcquisitionRollersSubsystem mAcquisitionRollersSubsystem;
  private boolean mIsfinished = false;

  public ArmDownThenIntakeCommand(AcquisitionArmSubsystem acquisitionArmSubsystem, CarouselSubsystem carouselSubsystem,
      AcquisitionRollersSubsystem acquisitionRollersSubsystem) {
    mAquisitionArmSubsystem = acquisitionArmSubsystem;
    mAcquisitionRollersSubsystem = acquisitionRollersSubsystem;
    addCommands(new ArmDownCommand(acquisitionArmSubsystem), 
      new IntakeCommand(carouselSubsystem, acquisitionRollersSubsystem));
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    super.initialize();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    super.execute();
    //TODO: re-enable this once we understand why code crashes when >= 5
    /* if (mAcquisitionRollersSubsystem.getNumBalls() >= 5) {
      mIsfinished = true;
    } */
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return mIsfinished;
  }
}
