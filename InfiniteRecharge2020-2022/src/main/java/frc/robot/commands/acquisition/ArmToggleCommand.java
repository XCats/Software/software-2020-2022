/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.acquisition;

import frc.robot.subsystems.AcquisitionArmSubsystem;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * An example command that uses an Aquisition Subsystem.
 */
public class ArmToggleCommand extends CommandBase {
  @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })
  private final AcquisitionArmSubsystem mAquisitionArmSubsystem;
  private boolean mIsFinished = false;

  /**
   * Creates a new Arm Toggle COmmand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public ArmToggleCommand(AcquisitionArmSubsystem acquisitionArmSubsystem) {
    mAquisitionArmSubsystem = acquisitionArmSubsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(acquisitionArmSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("ArmToggleCommand - Initialize");
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("ArmToggleCommand - Execute, toggling arm");
    mAquisitionArmSubsystem.toggle();
    mIsFinished = true;
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    System.out.println("ArmToggleCommand - End");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return mIsFinished;
  }
}
