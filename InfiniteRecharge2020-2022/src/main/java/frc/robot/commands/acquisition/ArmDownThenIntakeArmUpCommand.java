/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.acquisition;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.AcquisitionArmSubsystem;
import frc.robot.subsystems.AcquisitionRollersSubsystem;
import frc.robot.subsystems.CarouselSubsystem;

/**
 * An example command that uses an example subsystem.
 */
public class ArmDownThenIntakeArmUpCommand extends SequentialCommandGroup {
  @SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.SingularField" })

  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public ArmDownThenIntakeArmUpCommand(AcquisitionArmSubsystem acquisitionArmSubsystem,
      CarouselSubsystem carouselSubsystem, AcquisitionRollersSubsystem acquisitionRollersSubsystem) {
    addCommands(new ArmDownThenIntakeCommand(acquisitionArmSubsystem, carouselSubsystem, acquisitionRollersSubsystem),
        new ArmUpCommand(acquisitionArmSubsystem));
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    super.initialize();
    System.out.println("ArmDownThenIntakeArmUpCommand -- initialized");
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
    System.out.println("ArmDownThenIntakeArmUpCommand -- end");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}