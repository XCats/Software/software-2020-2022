/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import java.util.concurrent.atomic.AtomicInteger;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.ColorMatch;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.auto.AutoSelector;
import frc.robot.commands.ExampleCommand;
import frc.robot.commands.acquisition.ArmDownThenIntakeArmUpCommand;
import frc.robot.commands.acquisition.ArmToggleCommand;
import frc.robot.commands.acquisition.UnJamIntakeCommand;
import frc.robot.commands.carousel.CarouselCommand;
import frc.robot.commands.climber.DeployClimberCommand;
import frc.robot.commands.climber.RunClimberCommand;
import frc.robot.commands.controlPanel.ControlPanelMatchColorCommand;
import frc.robot.commands.controlPanel.ControlPanelSpinCommand;
import frc.robot.commands.drive.DefaultDriveBaseCommand;
import frc.robot.commands.drive.ShiftGearsCommand;
import frc.robot.commands.limelight.ToggleLimelightLEDsCommand;
import frc.robot.commands.shooter.ShootAndRunCarouselCommand;
import frc.robot.commands.shooter.TuneShooterRPMCommand;
import frc.robot.commands.shooter.TurnToTargetAndShootCommand;
import frc.robot.commands.turret.TurretDefaultCommand;
import frc.robot.commands.turret.TurretPIDCommand;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.AcquisitionArmSubsystem;
import frc.robot.subsystems.AcquisitionRollersSubsystem;
import frc.robot.subsystems.CarouselSubsystem;
import frc.robot.subsystems.ClimbingSubsystem;
import frc.robot.subsystems.ControlPanelSubsystem;
import frc.robot.subsystems.ExampleSubsystem;
import frc.robot.subsystems.InfiniteRechargeDriveBase;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.TurretSubsystem;
import frc.robot.subsystems.drive.TankDriveBase;
import frc.robot.utils.XboxButtonMap;
import frc.robot.utils.controllers.REVSparkMaxSpeedController;
import frc.robot.utils.controllers.WPITalonSRKSpeedController;
import frc.robot.utils.controllers.XSpeedController;
import frc.robot.utils.sensors.XEncoder;
import frc.robot.utils.sensors.XEncoder.Units;
import frc.robot.utils.sensors.XInu;
import frc.robot.utils.sensors.encoders.XSparkMaxEncoder;
import frc.robot.utils.sensors.impl.ColorSensorWrapper;
import frc.robot.utils.sensors.impl.PigeonWrapper;
import frc.robot.utils.sensors.impl.XCounter;
import frc.robot.utils.triggers.StickTrigger;
import frc.robot.utils.triggers.XboxButton;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    // The robot's subsystems and commands are defined here...

    // global values
    private final AtomicInteger mNumPowerCells = new AtomicInteger(3);

    /**
     * Joysticks & Buttons
     */
    private final XboxController mOperator = new XboxController(PortMappings.OPERATOR_JS);
    private final Joystick mLeftStick = new Joystick(PortMappings.LEFT_DRIVER_JS);
    private final Joystick mRightStick = new Joystick(PortMappings.RIGHT_DRIVER_JS);

    private final JoystickButton mLeftTriggerShiftGearsButton = new JoystickButton(mLeftStick, 1);
    private final JoystickButton mLeftNinthTuneRPM = new JoystickButton(mLeftStick, 9);
    private final JoystickButton mLeftTenthToggleLimelightLEDsButton = new JoystickButton(mLeftStick, 10);
    private final JoystickButton mLeftEleventhTurretTargetingButton = new JoystickButton(mLeftStick, 11);

    private final JoystickButton mRightTriggerButton = new JoystickButton(mRightStick, 1);
    private final JoystickButton mRightThirdColorSensorButton = new JoystickButton(mRightStick, 3);
    private final JoystickButton mRightEleventhAcquisitionButton = new JoystickButton(mRightStick, 11);

    private final XboxButton mXboxKXCarouselIntakeMode = new XboxButton(mOperator, XboxController.Button.kX);
    private final XboxButton mXboxUnJamIntake = new XboxButton(mOperator, XboxController.Button.kY);
    private final XboxButton mXboxStartWinchButton = new XboxButton(mOperator, XboxController.Button.kStart);
    private final XboxButton mXboxBackClimberButton = new XboxButton(mOperator, XboxController.Button.kBack);
    private final XboxButton mXboxKAShoot = new XboxButton(mOperator, XboxController.Button.kA);
    private final XboxButton mXboxKBArmToggle = new XboxButton(mOperator, XboxController.Button.kB);
    private final XboxButton mXboxLeftBumperControlPanelMatchColor = new XboxButton(mOperator,
            XboxController.Button.kLeftBumper);
    private final XboxButton mXboxRightBumperControlPanelSpin = new XboxButton(mOperator,
            XboxController.Button.kRightBumper);
    private final StickTrigger mXboxLeftTriggerToggleLEDSButton = new StickTrigger(mOperator, XboxButtonMap.LEFT_TRIGGER);

    /*
     * Controllers
     */

    // Color sensor port
    private final I2C.Port i2cPort = I2C.Port.kOnboard;

    // Acquisition
    private final DoubleSolenoid mAcquisitionArm = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, PortMappings.DEPLOY_ACQ_IN_SOLENOID,
            PortMappings.DEPLOY_ACQ_OUT_SOLENOID);
    private final WPI_TalonSRX mAcquisitionSideToSide = new WPI_TalonSRX(PortMappings.ACQ_SIDE2SIDE_BELT_MOTOR);
    private final WPI_TalonSRX mAcquisitionFrontToBack = new WPI_TalonSRX(PortMappings.ACQ_FRONT2BACK_BELT_MOTOR);
    private final XSpeedController mAcquisitionSideToSideSpeedController = new WPITalonSRKSpeedController(
            mAcquisitionSideToSide);
    private final XSpeedController mAcquisitionFrontToBackController = new WPITalonSRKSpeedController(
            mAcquisitionFrontToBack);
    private final XCounter mBallIntakeSensor = new XCounter(PortMappings.BALL_IN_ACQUISITION, "BallIntakeSensor");
    private final XCounter mBallInCarouselSensor = new XCounter(PortMappings.BALL_IN_CAROUSEL, "BallInCarouselSensor");

    // Drive
    private final DoubleSolenoid mGearShift = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, PortMappings.DRIVES_GEARBOX_OUT_SOLENOID,
        PortMappings.DRIVES_GEARBOX_IN_SOLENOID);
    private final CANSparkMax mLeftMaster = new CANSparkMax(PortMappings.DRIVE_MOTOR_LEFT_MASTER, MotorType.kBrushless);
    private final CANSparkMax mRightMaster = new CANSparkMax(PortMappings.DRIVE_MOTOR_RIGHT_MASTER, MotorType.kBrushless);
    private final CANSparkMax mLeftFollower = new CANSparkMax(PortMappings.DRIVE_MOTOR_LEFT_FOLLOWER, MotorType.kBrushless);
    private final CANSparkMax mRightFollower = new CANSparkMax(PortMappings.DRIVE_MOTOR_RIGHT_FOLLOWER, MotorType.kBrushless);
    private final XSpeedController mRightSpeedController = new REVSparkMaxSpeedController(mRightMaster);
    private final XSpeedController mLeftSpeedController = new REVSparkMaxSpeedController(mLeftMaster);
    private final XSparkMaxEncoder mRightEncoder = new XSparkMaxEncoder("Left Drive Encoder", mLeftMaster,
        Constants.DRIVETRAIN_DISTANCE_PER_ROTATION, Units.Inches,
        Constants.DRIVETRAIN_ENCODER_TPR, true);
    private final XSparkMaxEncoder mLeftEncoder = new XSparkMaxEncoder("Right Drive Encoder", mRightMaster,
        Constants.DRIVETRAIN_DISTANCE_PER_ROTATION, Units.Inches,
        Constants.DRIVETRAIN_ENCODER_TPR);

    // climber
    private final CANSparkMax mClimbingMotor = new CANSparkMax(PortMappings.CLIMBER_WINCH_MOTOR, MotorType.kBrushless);
    private final REVSparkMaxSpeedController mClimbingSpeedController = new REVSparkMaxSpeedController(mClimbingMotor);
    private final DoubleSolenoid mClimberDeploy = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, PortMappings.CLIMBER_OUT_SOLENOID,
            PortMappings.CLIMBER_IN_SOLENOID);

    // Control Panel
    private final DoubleSolenoid mControlPanelArm = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, PortMappings.DEPLOY_SPINNER_OUT_SOLENOID,
            PortMappings.DEPLOY_SPINNER_IN_SOLENOID);
    private final CANSparkMax mControlPanelMotor = new
      CANSparkMax(PortMappings.CP_SPINNER_MOTOR, MotorType.kBrushless);
    private final XSpeedController mControlPanelSpeedController = new 
      REVSparkMaxSpeedController(mControlPanelMotor);

    // Turret
    private final CANSparkMax mTurretMotor = new CANSparkMax(PortMappings.TURRET_MOTOR, MotorType.kBrushless);
    private final REVSparkMaxSpeedController mTurretSpeedController = new REVSparkMaxSpeedController(mTurretMotor);
    private final XSparkMaxEncoder mTurretEncoder = new XSparkMaxEncoder("Turret Encoder", mTurretMotor,
    Constants.TURRET_DISTANCE_PER_ROTATION, Units.Inches);


    // Shooter
    private final CANSparkMax mShooterMotor = new CANSparkMax(PortMappings.SHOOTER_MOTOR, MotorType.kBrushless);
    private final REVSparkMaxSpeedController mShooterSpeedController = new REVSparkMaxSpeedController(mShooterMotor);
    private final WPI_TalonSRX mFeederMotor = new WPI_TalonSRX(PortMappings.SHOOTER_FEED_WHEEL_MOTOR);
    private final WPITalonSRKSpeedController mFeederSpeedController = new WPITalonSRKSpeedController(mFeederMotor);
    private final XEncoder mShooterEncoder = new XSparkMaxEncoder("Shooter Encoder", mShooterMotor, 0.00, Units.Inches);

    // Carousel
    private final WPI_TalonSRX mCarouselMotor = new WPI_TalonSRX(PortMappings.CAROUSEL_MOTOR);
    private final WPITalonSRKSpeedController mCarouselSpeedController = new WPITalonSRKSpeedController(mCarouselMotor);

    // global values
    private final AtomicInteger mNumBalls = new AtomicInteger(3);

    /**
     * Utilities
     */
    private final ColorSensorV3 mColorSensor = new ColorSensorV3(i2cPort);
    private final ColorMatch mColorMatcher = new ColorMatch();
    private final ColorSensorWrapper mColorSensorWrapper = new ColorSensorWrapper(mColorSensor, mColorMatcher);

    /**
     * Subsystems
     */
    private final TurretSubsystem mTurretSubsystem = new TurretSubsystem(mTurretSpeedController, mTurretEncoder);
    private final TankDriveBase mDriveSubsystem = new
        InfiniteRechargeDriveBase(mLeftSpeedController,
            mRightSpeedController, mRightEncoder, mLeftEncoder, mGearShift);
    private final ShooterSubsystem mShooterSubsystem = new ShooterSubsystem(mShooterSpeedController,
            mFeederSpeedController, mNumPowerCells, mShooterEncoder);
    private final CarouselSubsystem mCarouselSubsystem = new CarouselSubsystem(mCarouselSpeedController);
    private final AcquisitionArmSubsystem mAcquisitionArmSubsystem = new AcquisitionArmSubsystem(mAcquisitionArm);
    private final AcquisitionRollersSubsystem mAcquisitionRollersSubsystem = new AcquisitionRollersSubsystem(
            mAcquisitionSideToSideSpeedController, mAcquisitionFrontToBackController, mNumBalls, 
            mBallIntakeSensor, mBallInCarouselSensor);
    private final ClimbingSubsystem mClimbingSubsystem = new ClimbingSubsystem(mClimberDeploy,
            mClimbingSpeedController);
    private final ControlPanelSubsystem mControlPanelSubsystem = new
      ControlPanelSubsystem(mControlPanelSpeedController,
      mColorSensorWrapper, mControlPanelArm);
    
    // processors
    private final XInu mInu = new PigeonWrapper(mAcquisitionFrontToBack);
    // mpk private final Positioner mPositioner = new Positioner(mDriveSubsystem,
    // mInu);
    // mpk private final Limelight mLimelight = new Limelight(mPositioner); // for
    // 2019
    private final Limelight mLimelight = new Limelight(); // for 2020, w/ turret

    // Examples
    private final ExampleSubsystem mExampleSubsystem = new ExampleSubsystem();
    private final ExampleCommand mAutoCommand = new ExampleCommand(mExampleSubsystem);

    /**
     * The container for the robot. Contains subsystems, OI devices, and commands.
     */
    public RobotContainer() {

        mLeftFollower.follow(mLeftMaster); mRightFollower.follow(mRightMaster);

        // Configure the button bindings
        configureButtonBindings();

        // Set default commands for subsystems
        setSubsystemDefaultCommands();
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be
     * created by instantiating a {@link GenericHID} or one of its subclasses
     * ({@link edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then
     * passing it to a {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {

        // RIGHT JOYSTICK
        mRightTriggerButton
                .whileHeld(new ShootAndRunCarouselCommand(mShooterSubsystem, mLimelight, mCarouselSubsystem));
        
        // mRightTriggerButton
        // .whileHeld(new ShootBasic(mShooterSubsystem).alongWith(new CarouselCommand(mCarouselSubsystem, CarouselSubsystem.Mode.Shoot)));
        mRightEleventhAcquisitionButton.toggleWhenActive(new ArmDownThenIntakeArmUpCommand(mAcquisitionArmSubsystem,
                mCarouselSubsystem, mAcquisitionRollersSubsystem));
//        mRightThirdColorSensorButton.whenPressed(new RunCommand(() -> mColorSensorWrapper.isMatch()));

        // LEFT JOYSTICK
        mLeftTriggerShiftGearsButton.whenPressed(new ShiftGearsCommand(mDriveSubsystem));
        mLeftNinthTuneRPM.whenActive(new TuneShooterRPMCommand(mShooterSubsystem).raceWith(new CarouselCommand(mCarouselSubsystem, CarouselSubsystem.Mode.Shoot)));
        mLeftTenthToggleLimelightLEDsButton.whenReleased(new ToggleLimelightLEDsCommand(mLimelight));
        mLeftEleventhTurretTargetingButton.whenActive(new TurretPIDCommand(mTurretSubsystem, mLimelight));

        // XBOX
        mXboxKXCarouselIntakeMode.whileActiveContinuous(new CarouselCommand(mCarouselSubsystem, CarouselSubsystem.Mode.Intake));
        mXboxUnJamIntake.whileActiveContinuous(new UnJamIntakeCommand(mCarouselSubsystem, mAcquisitionRollersSubsystem));
//        mXboxStartWinchButton.whileActiveContinuous(new RunClimberCommand(mClimbingSubsystem));
//        mXboxBackClimberButton.toggleWhenActive(new DeployClimberCommand(mClimbingSubsystem));
        mXboxKAShoot.whileActiveOnce(
                new TurnToTargetAndShootCommand(mTurretSubsystem, mLimelight, mShooterSubsystem, mCarouselSubsystem));
        mXboxKBArmToggle.whileActiveOnce(new ArmToggleCommand(mAcquisitionArmSubsystem));
//        mXboxRightBumperControlPanelSpin.toggleWhenActive(new
//          ControlPanelSpinCommand(mControlPanelSubsystem));
//        mXboxLeftBumperControlPanelMatchColor.toggleWhenActive(new
//          ControlPanelMatchColorCommand(mControlPanelSubsystem));
    }

    private void setSubsystemDefaultCommands() {
        mTurretSubsystem.setDefaultCommand(new TurretDefaultCommand(mTurretSubsystem, mOperator));
        mDriveSubsystem.setDefaultCommand(new DefaultDriveBaseCommand(mDriveSubsystem, mLeftStick, mRightStick));
    }

    private AutoSelector mAutoSelector = new AutoSelector(mDriveSubsystem, mTurretSubsystem, mShooterSubsystem, mLimelight);

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        // An ExampleCommand will run in autonomous
        return mAutoSelector.getSelectedCommand();
    }

    public void setMatchColor(String matchColor) {

        mColorSensorWrapper.setMatchColor(matchColor);
    }

    public void initialize() {
        //Drivetrain
        IdleMode motorMode = IdleMode.kBrake;
        mRightMaster.getAlternateEncoder(4096);
        mLeftMaster.getAlternateEncoder(4096);
        mRightMaster.setIdleMode(motorMode);
        mLeftMaster.setIdleMode(motorMode);
        mRightFollower.setIdleMode(motorMode);
        mLeftFollower.setIdleMode(motorMode);
        mLeftFollower.follow(mLeftMaster);
        mRightFollower.follow(mRightMaster);
        
        //Shooter
        mShooterMotor.restoreFactoryDefaults();
        mShooterMotor.clearFaults();
        mShooterMotor.setInverted(false);

        //Climber
        mClimbingMotor.setIdleMode(IdleMode.kCoast);   
    }

}
