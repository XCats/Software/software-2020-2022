package frc.robot.processors;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Encapsulation of Limelight2 functionality. For 2020 with moveable turret, no
 * Positioner is necessary.
 */
public class Limelight extends SubsystemBase {

	public enum LEDState {
		OFF, ON;
	}

	// private final Positioner mPositioner;
	private final NetworkTable mLimelightTable;

	private boolean mTargetSeen;
	private double mRawDistance;
	private double mRobotDistance;
	private double mShooterDistance;
	private double mAngle;
	private double mX;
	private double mY;
	private LEDState mLEDState = LEDState.OFF;

	private int mpkTestPrint = 0;

	private double mRawTargetArea;
	private double mRawPitchError;
	private double mRawYawError;

	@Override
	public void initSendable(SendableBuilder builder) {
		builder.addBooleanProperty("Sees Target", Limelight.this::seesTarget, null);
		builder.addDoubleProperty("Target X", Limelight.this::getTargetX, null);
		builder.addDoubleProperty("Target Y", Limelight.this::getTargetY, null);
		builder.addDoubleProperty("Distance", Limelight.this::getDistance, null);
		builder.addDoubleProperty("Angle", Limelight.this::getAngle, null);
	}

	// public Limelight(Positioner position) {
	public Limelight() {
		// mPositioner = position;
		mLimelightTable = NetworkTableInstance.getDefault().getTable("limelight");
		mLimelightTable.getEntry("ledMode").setDouble(mLEDState.ordinal());
	}

	@Override
	public void periodic() {

		mTargetSeen = mLimelightTable.getEntry("tv").getDouble(0.0) == 1.0;
		mRawTargetArea = mLimelightTable.getEntry("ta").getDouble(0.0);
		mRawPitchError = mLimelightTable.getEntry("ty").getDouble(0);
		mRawYawError = mLimelightTable.getEntry("tx").getDouble(0);

		if (mTargetSeen) {
			double camerAngle = mRawPitchError;
			//double mpkElAngle = 32; // mpk - not reading Constants right. TODO: remove when ready.
			//double elAngle = mpkElAngle + camerAngle; // TODO: remove when ready.
			double elAngle = Constants.CAMERA_EL_ANGLE_OFFSET.getValue() + camerAngle;
			double rayDistance = Math.tan(Math.toRadians(elAngle));
			mRawDistance = Constants.CAMERA_TO_TARGET_HEIGHT / rayDistance;
			mRobotDistance = mRawDistance - Constants.CAMERA_DIST_TO_BUMPER;
			mShooterDistance = mRawDistance + Constants.CAMERA_DIST_TO_SHOOTER_EXIT;
			mAngle = mRawYawError;
			if (++mpkTestPrint >= 500) { // i.e. update every 10 seconds or so
				System.out.println("LL2 total elev angle = " + elAngle 
				  + "    robot distance = " + mRobotDistance);
				mpkTestPrint = 0;
			}
			mX = Double.MAX_VALUE;
			mY = Double.MAX_VALUE;
			// mAngle = mPositioner.getAngle() + mRawYawError;
			// mX = mPositioner.getX() + (Math.sin(Math.toRadians(mAngle)) * mRawDistance);
			// mY = mPositioner.getY() + (Math.cos(Math.toRadians(mAngle)) * mRawDistance);

			// String positionsText = mPositioner.getX() + "," + mPositioner.getY() + "," +
			// mX + "," + mY;
		} else {
			mRawDistance = Double.MAX_VALUE;
			mRobotDistance = mShooterDistance = Double.MAX_VALUE;
			mAngle = Double.MAX_VALUE;
			mX = Double.MAX_VALUE;
			mY = Double.MAX_VALUE;
		}
	}

	public boolean seesTarget() {
		return mTargetSeen;
	}

	public double getDistance() {
		return mRobotDistance;
	}

	public double getAngle() {
		return mAngle;
	}

	public double getTargetX() {
		return mX;
	}

	public double getTargetY() {
		return mY;
	}

	public void turnOffLeds() {
		System.out.println("Limelight -- Turn LEDs Off");
		mLEDState = LEDState.OFF;
		setLEDState();
	}

	public void turnOnLeds() {
		System.out.println("Limelight -- Turn LEDs On");
		mLEDState = LEDState.ON;
		setLEDState();
	}

	public void toggleLEDS() {
		if (mLEDState == LEDState.OFF) {
			turnOnLeds();
		} else {
			turnOffLeds();
		}
	}

	public boolean isLEDOn() {
		return mLEDState == Limelight.LEDState.ON;
	}

	public double getRawTargetArea() {
		return mRawTargetArea;
	}

	public double getRawPitchError() {
		return mRawPitchError;
	}

	public double getRawYawError() {
		return mRawYawError;
	}

	private void setLEDState() {
		mLimelightTable.getEntry("ledMode").setDouble(mLEDState.ordinal());
	}
}
