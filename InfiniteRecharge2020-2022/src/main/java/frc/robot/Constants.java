/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.utils.PropertyManager.IProperty;

import java.util.TreeMap;

import com.revrobotics.ColorMatch;

import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.math.util.Units;
import frc.robot.utils.InterpolatingTreeMap;
import frc.robot.utils.PropertyManager;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    public static final double GROUND_TO_TARGET = 98.25 - 8.5;
    public static final double GROUND_TO_CAMERA = 21.5;
    public static final double CAMERA_TO_TARGET_HEIGHT = GROUND_TO_TARGET - GROUND_TO_CAMERA; // Inches. 2019 value was -15.75
    public static final double CAMERA_DIST_TO_BUMPER = 6.5; // Inches.
    public static final double CAMERA_DIST_TO_SHOOTER_EXIT = 10.5; // Inches.
    public static final double TEST_DISTANCE_VALUE = 45 + CAMERA_DIST_TO_BUMPER; //value added is the value obtained with bumpers
    public static final double TEST_DEGREES_VALUE = 19.26; //tY value limelight gives us (degrees)
    public static final IProperty<Double> CAMERA_EL_ANGLE_OFFSET = new PropertyManager.DoubleProperty("CameraAngleOffset", Math.toDegrees(Math.atan((CAMERA_TO_TARGET_HEIGHT) / TEST_DISTANCE_VALUE)) - TEST_DEGREES_VALUE); // Estimated. Angle, in degrees
    
    //Drivetrain
    public static final double WHEEL_DIAMETER = 6.0; // 2020 ; in inches.  For 2019 was 7.375"
    public static final double DRIVETRAIN_DISTANCE_PER_ROTATION = WHEEL_DIAMETER * Math.PI; // C = pi * d, in inches. Used on startup, so no point in making a property
    public static final double DRIVETRAIN_ENCODER_TPR = 4096;
    public static final double ROBOT_LENGTH = 36.0; // Inches. Bumper to bumper.
    public static final double ROBOT_WIDTH = 33.0; // Inches. Bumper to bumper.
    public static final double AUTO_SPEED = -0.5;

    
    //Turret
    //  42 ticks per rot. 
    public static final double TURRET_ENCODER_TPR = 42;
    public static final double TURRET_GEAR_RATIO = 24; // 24:1 for final bot, 81:1 for proto
    public static final double TURRET_GEAR_PITCH_DIAMETER = 0.809; // dia. of gear turning turret wheel
    public static final double TURRET_PITCH_DIAMETER = 13.036; // dia. of turret wheel itself
    public static final double TURRET_DISTANCE_PER_ROTATION = TURRET_GEAR_PITCH_DIAMETER * Math.PI; // C = pi * d, in inches.
    public static final double DEFAULT_TURRET_TIMER_OVERRDIE = 5.0; //seconds
    //.public static final double TURRET_TICKS_PER_DEGREE = (42 * 81 * 13.036) / (.809 * 360);
    public static final double TURRET_TICKS_PER_DEGREE = (TURRET_ENCODER_TPR * TURRET_GEAR_RATIO * TURRET_PITCH_DIAMETER) 
        / (TURRET_GEAR_PITCH_DIAMETER * 360);
    // on proto public static final double TURRET_TICKS_PER_DEGREE = (42 * 81 * 13.036) / (.809 * 360);    
    
    //Aquisition
    // public static final IProperty<Double> AQUISITION_SIDE_TO_SIDE_SPEED = new PropertyManager.DoubleProperty("Aqu.FrontToBackSpeed", 1.0); //out of 1.0
    // public static final IProperty<Double> AQUISITION_FRONT_TO_BACK_SPEED = new PropertyManager.DoubleProperty("Aqu.SideToSideSpeed", 1.0); //out of 1.0
    public static final double AQUISITION_SIDE_TO_SIDE_SPEED = 1; //out of 1.0
    public static final double AQUISITION_FRONT_TO_BACK_SPEED = 1; //out of 1.0
    
    //Control Panel Colors
    public static final IProperty<Color> BLUE_CONTROL_PANEL_COLOR = new PropertyManager.ConstantProperty<Color>("BlueControlPanelColor", new Color(0.280003, 0.47998, 0.23999));
    public static final IProperty<Color> GREEN_CONTROL_PANEL_COLOR = new PropertyManager.ConstantProperty<Color>("GreenControlPanelColor", new Color(0.333252, 0.523926, 0.142822));
    public static final IProperty<Color> RED_CONTROL_PANEL_COLOR = new PropertyManager.ConstantProperty<Color>("RedControlPanelColor", new Color(0.555664, 0.370361, 0.073975));
    public static final IProperty<Color> YELLOW_CONTROL_PANEL_COLOR = new PropertyManager.ConstantProperty<Color>("YellowControlPanelColor", new Color(0.435791, 0.487061, 0.007694));

    //Shooter
    public static final IProperty<Double> SHOOTER_SPINUP_RPM = new PropertyManager.DoubleProperty("ShooterSpinupRPM", 800);
    public static final IProperty<Double> DEFAULT_SHOOTER_RPM = new PropertyManager.DoubleProperty("DefaultShooterRPM", 3425.0);
    public static final IProperty<Double> DEFAULT_FEEDER_MOTOR_SPEED = new PropertyManager.DoubleProperty("DefaultFeederMotorSpeed", .85); //out of 1.0
    public static final double DEFAULT_SHOOTER_OUTPUT = 0.5;
    public static final InterpolatingTreeMap SHOOTER_TREE_MAP = new InterpolatingTreeMap(new TreeMap<Double, Double>(), 0)
        .putValue(50.6, 2550)
        .putValue(66.2, 2450)
        .putValue(76.9, 2400)
        .putValue(90.1, 2550)
        .putValue(150.5, 2700)
        .putValue(195.5, 3000);

    public static final IProperty<Double> SHOOTER_MAX_RPM = new PropertyManager.DoubleProperty("ShooterMaxRPM", 5700);
    public static final IProperty<Double> SHOOTER_MAX_VOLTAGE = new PropertyManager.DoubleProperty("ShooterMaxVoltage", 12);

    public static final IProperty<Double> SHOOTER_ACCEL_COEFF = new PropertyManager.DoubleProperty("ShooterAccelCoeff", 0.3);
    public static final IProperty<Double> DEFAULT_SHOOTER_KP = new PropertyManager.ConstantProperty<Double>("DefaultShooterKP", 1.16/(SHOOTER_MAX_VOLTAGE.getValue() * 60));
    public static final IProperty<Double> DEFAULT_SHOOTER_KI = new PropertyManager.DoubleProperty("DefaultShooterKI", 0.0);
    public static final IProperty<Double> DEFAULT_SHOOTER_KD = new PropertyManager.DoubleProperty("DefaultShooterKD", 0.0);
    // public static final IProperty<Double> DEFAULT_SHOOTER_KF = new PropertyManager.DoubleProperty("DefaultShooterKF", 0.000175);
    
    //Joystick deadband
    public static final IProperty<Double> DEFAULT_JOYSTICK_DEADBAND = new PropertyManager.DoubleProperty("DefaultDeadband", .15); //out of 1.0
    // The top speed that will come out of drive smoother util
    public static final IProperty<Double> DEFAULT_JOYSTICK_TOPSPEED = new PropertyManager.DoubleProperty("DefaultTopSpeed", 1.0); // DEADBAND < x <= 1.0
    // The minimum increment to raise/lower speed per execute cycle
    public static final IProperty<Double> DEFAULT_JOYSTICK_MIN_INCR = new PropertyManager.DoubleProperty("DefaultSpeedMinIncr", 0.04); // 0.02 works ok

    //PID Controller
    public static final IProperty<Integer> DEFAULT_PID_SLOT = new PropertyManager.IntProperty("DefaultPIDSlot", 0); //cheemz

    //Turret Commands
    public static final IProperty<Double> TURRET_ALLOWABLE_ERROR = new PropertyManager.DoubleProperty("TurretAllowableError", 0.5); // was 1.0
    public static final IProperty<Double> TURRET_SPEED = new PropertyManager.DoubleProperty("TurretSpeed", .06); // was .1
    public static final IProperty<Double> TURRET_AUTO_SPEED = new PropertyManager.DoubleProperty("TurretAutoSpeed", .1); // was .6
    public static final IProperty<Double> TURRET_KP = new PropertyManager.DoubleProperty("TurretKP", .006); // was .06   

    //Carousel motor speed
    // 'tested good' value on 3/8/20 is 1.0
    // public static final IProperty<Double> CAROUSEL_INTAKE_SPEED = new PropertyManager.DoubleProperty("CarouselIntakeSpeed", 1.0);
    // public static final IProperty<Double> CAROUSEL_SHOOT_SPEED = new PropertyManager.DoubleProperty("CarouselShootSpeed", 1.0);
    public static final double CAROUSEL_INTAKE_SPEED = 1.0;
    public static final double CAROUSEL_SHOOT_SPEED = 1.0;
    public static final double CAROUSEL_ANTI_JAM = -1.0;
    
    //Climber
    public static final IProperty<Double> CLIMBER_SPEED = new PropertyManager.DoubleProperty("ClimberSpeed", 0.5);

    //Motors
    public static final IProperty<Double> MOTOR_STOP_SPEED = new PropertyManager.DoubleProperty("MotorStopPercentageSpeed", 0.0);

    //Constants for ramsete
    public static final double KS_VOLTS = 0.22;
    public static final double KV_VOLT_SECONDS_PER_METER = 1.98;
    public static final double KS_VOLTS_SECONDS_SQUARED_PER_METER = 0.2;
    public static final double K_TRACK_WIDTH_METERS = Units.inchesToMeters(40);
    public static final DifferentialDriveKinematics K_DRIVE_KINEMATICS =
        new DifferentialDriveKinematics(K_TRACK_WIDTH_METERS);
    public static final double K_MAX_SPEED_METERS_PER_SECOND = Units.inchesToMeters(120);
    public static final double K_MAX_ACCELERATION_METERS_PER_SECOND_SQUARED = Units.inchesToMeters(120);
    public static final double K_RAMSETE_B = 2;
    public static final double K_RAMSETE_ZETA = 0.7;
}

