package frc.robot.utils.sensors.encoders;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import frc.robot.utils.sensors.XEncoder;
import frc.robot.utils.sensors.impl.XBaseEncoder;

public class XSparkMaxEncoder extends XBaseEncoder {
	private static final String ENCODER_TYPE = "SparkMax";
	private static final double ENCODER_TPR = 42; // Encoder ticks per revolution

	private final CANEncoder mEncoder;

	public XSparkMaxEncoder(String name, CANSparkMax sparkMax, double distancePerRotation, XEncoder.Units distanceUnit) {
		super(name, ENCODER_TYPE, ENCODER_TPR, distancePerRotation, distanceUnit);
		mEncoder = sparkMax.getEncoder();
	}

	/**use this constructer when wanting to use spark max alternate encoder */
	public XSparkMaxEncoder(String name, CANSparkMax sparkMax, double distancePerRotation, XEncoder.Units distanceUnit,
			double ticksPerRotation) {
		super(name, "CTRE MAG", ticksPerRotation, distancePerRotation, distanceUnit);
		mEncoder = sparkMax.getAlternateEncoder((int) ticksPerRotation);
	}

	/**use this constructer when wanting to use spark max alternate encoder */
	public XSparkMaxEncoder(String name, CANSparkMax sparkMax, double distancePerRotation, XEncoder.Units distanceUnit,
		double ticksPerRotation, boolean isInverted) {
		this(name, sparkMax, distancePerRotation, distanceUnit, ticksPerRotation);
		mEncoder.setInverted(isInverted);
	}

	@Override
	public void zero() {
		this.mEncoder.setPosition(0);
	}

	@Override
	public double getRawDistanceTicks() {
		return this.mEncoder.getPosition();
	}

	@Override
	public double getRawVelocityTicks() {
		return this.mEncoder.getVelocity();
	}

}
