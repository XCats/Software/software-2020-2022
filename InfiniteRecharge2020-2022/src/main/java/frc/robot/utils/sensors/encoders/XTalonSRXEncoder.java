package frc.robot.utils.sensors.encoders;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import frc.robot.utils.sensors.impl.XBaseEncoder;

public class XTalonSRXEncoder extends XBaseEncoder {
	private static final String ENCODER_TYPE = "TalonSRX";
	private static final double ENCODER_TPR = 4096; // Encoder ticks per revolution

	private final WPI_TalonSRX mTalon;

	public XTalonSRXEncoder(String name, WPI_TalonSRX talon, double distancePerRotation, Units distanceUnit) {
		super(name, ENCODER_TYPE, ENCODER_TPR, ENCODER_TPR / 10, distancePerRotation, distanceUnit);
		this.mTalon = talon;

		this.mTalon.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative);
	}

	@Override
	public void zero() {
		this.mTalon.getSensorCollection().setQuadraturePosition(0, 0);
	}

	@Override
	public double getRawDistanceTicks() {
		return mTalon.getSelectedSensorPosition();
	}

	@Override
	public double getRawVelocityTicks() {
		return mTalon.getSelectedSensorVelocity();
	}
}
