package frc.robot.utils.sensors;

import edu.wpi.first.util.sendable.Sendable;

public interface XEncoder {

    enum Units {
        Inches, Feet;
    }

    /**
     * Gets the raw distance the sensor, with no conversion done. Units depend on
     * the sensor.
     *
     * @return The distance in native units
     */
    double getRawDistanceTicks();

    /**
     * Gets the raw velocity from the sensor, with no conversion done. Units depend
     * on the sensor
     *
     * @return The velocity in native units
     */
    double getRawVelocityTicks();

    /**
     * Gets the number of rotations the encoder has traveled
     *
     * @return The number of rotations
     */
    double getRotationalDistance();

    /**
     * Gets rotational velocity of the sensor, in rotations/sec
     *
     * @return The velocity in rot/sec
     */
    double getRotationalVelocity();

    /**
     * Gets the velocity in RPM
     *
     * @return Velocity in RPM
     */
    double getRPM();

    /**
     * Gets the linear distance traveled by the sensor. The units are dependent on
     * the {@code mDistancePerRotation} constructor parameter
     *
     * @return The linear distance in {@link #getDistanceUnit()}
     */
    double getLinearDistance();

    /**
     * Gets the linear velocity of the sensor, in units/sec. Units are dependent on
     * how the sensor was set up. See {@link #getDistanceUnit()}
     *
     * @return The linear velocity in {@link #getDistanceUnit()}/second
     */
    double getLinearVelocity();

    /**
     * Resets the encoder so that the raw units will start counting from zero.
     */
    void zero();

    /**
     * Converts the given distance, in {@link #getDistanceUnit()} into native sensor ticks
     *
     * @param distance The distance, in real world units
     * @return encoder ticks
     */
    double convertToTicksFromDistance(double distance);

    /**
     * Gets the distance unit used for linear distance/velocity calculation. Note:
     * it is important that the sensors unit matches this string, otherwise
     * confusion will ensue.
     *
     * @return A string representation of the unit type
     */
    Units getDistanceUnit();

    /**
     * Gets the sendable representing the state of the encoder, for use with the
     * SmartDashboard. Note, this call will lazily instantiate the sendable so it
     * won't be present on the dashboard unless this is called.
     *
     * @return The encoder representation
     */
    Sendable getSendable();

}
