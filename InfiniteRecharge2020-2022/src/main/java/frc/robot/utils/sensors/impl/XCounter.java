package frc.robot.utils.sensors.impl;

import java.util.HashMap;
import java.util.Map;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import frc.robot.utils.NetworkTableHelper;

public class XCounter {
    private final DigitalInput mSensor;
    private boolean mSawObject = false;
    private NetworkTable mTable;
    private final String mName;
    
    public XCounter(int port, String counterName) {
        mSensor = new DigitalInput(port);
        mName = counterName;
        mTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable(mName);
    }

    public boolean seesObject () {
        boolean seesObject;
        
        if (mSensor.get()) {
            mSawObject = true;
        }

        if (mSawObject && mSensor.get() == false) {
            mSawObject = false;
            seesObject = true;

        } else {
            seesObject = false;
        }

        Map<String, Object> dataMap = new HashMap<String, Object>() {
            {
                put("Saw Object", mSawObject);
            }
        };

        NetworkTableHelper.putNetworkTableData(mTable, dataMap);

        return seesObject;
    }

}