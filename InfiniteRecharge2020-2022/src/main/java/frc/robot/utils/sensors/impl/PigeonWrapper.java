package frc.robot.utils.sensors.impl;

import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.sensors.PigeonIMU;

public class PigeonWrapper extends XBaseInu {

    private final PigeonIMU mPigeon;
    private final double[] mAngles = new double[3];

    public PigeonWrapper(TalonSRX canTalon) {
        this(canTalon, GyroDirection.Yaw);
    }

    public PigeonWrapper(TalonSRX canTalon, GyroDirection gyroDirection) {
        super("Pigeon IMU", gyroDirection);
        mPigeon = new PigeonIMU(canTalon);
    }

    @Override
    public double getYaw() {
        mPigeon.getAccumGyro(mAngles);
        return -super.getYaw();
    }


    @Override
    public double getRawYaw() {
        return mAngles[0];
    }

    @Override
    public double getRawPitch() {
        return mAngles[1];
    }

    @Override
    public double getRawRoll() {
        return mAngles[2];
    }

}
