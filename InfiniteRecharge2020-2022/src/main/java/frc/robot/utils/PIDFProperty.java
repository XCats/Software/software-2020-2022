package frc.robot.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import frc.robot.utils.PropertyManager.IProperty;

public class PIDFProperty {
    private List<AlmostConstantProperty<Double>> mProperties;

    private PIDFProperty(List<AlmostConstantProperty<Double>> aProperties) {
        mProperties = aProperties;
    }

    public void updateValueIfChanged(boolean force) {
        mProperties.stream().forEach(f -> f.updateValueIfChanged(force));
    }

    public void updateValueIfChanged() {
        this.updateValueIfChanged(false);
    }

    public static class Builder {
        private List<AlmostConstantProperty<Double>> mProperties;

        public Builder() {
            mProperties = new ArrayList<>();
        }

        public Builder addPProperty(IProperty<Double> property, Consumer<Double> setter) {
            mProperties.add(new AlmostConstantProperty<Double>(property, setter));
            return this;
        }

        public Builder addIProperty(IProperty<Double> property, Consumer<Double> setter) {
            mProperties.add(new AlmostConstantProperty<Double>(property, setter));
            return this;
        }

        public Builder addDProperty(IProperty<Double> property, Consumer<Double> setter) {
            mProperties.add(new AlmostConstantProperty<Double>(property, setter));
            return this;
        }

        public Builder addFProperty(IProperty<Double> property, Consumer<Double> setter) {
            mProperties.add(new AlmostConstantProperty<Double>(property, setter));
            return this;
        }

        public PIDFProperty toProperty() {
            return new PIDFProperty(mProperties);
        }
    }
}