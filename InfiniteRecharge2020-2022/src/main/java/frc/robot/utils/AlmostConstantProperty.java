package frc.robot.utils;

import java.util.function.Consumer;

import frc.robot.utils.PropertyManager.IProperty;

/**
 * This class is used for property's in CAN that don't need to be sent all of the time, values are
 * updated when they are changed
 */

public class AlmostConstantProperty<T> {
    PropertyManager.IProperty<T> mProperty;
    Consumer<T> mSetter;
    T mLastValue;

    public AlmostConstantProperty(PropertyManager.IProperty<T> property, Consumer<T> setter) {
        mProperty = property;
        mSetter = setter;
        mLastValue = mProperty.getValue();

    }

    public void updateValueIfChanged(boolean forceUpdate) {
        if (mLastValue != mProperty.getValue() || forceUpdate) {
            mSetter.accept(mProperty.getValue());
            mLastValue = mProperty.getValue();
        }

    }

    public void updateValueIfChanged() {
        updateValueIfChanged(false);
    }

    public IProperty<T> getProperty() {
        return this.mProperty;
    }

}