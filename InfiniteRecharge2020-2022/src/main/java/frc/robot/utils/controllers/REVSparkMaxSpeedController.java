package frc.robot.utils.controllers;

import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;

public class REVSparkMaxSpeedController implements XSpeedController {

    private CANSparkMax mSpeedController;
    private CANPIDController mPid;
    private double mMinOutput = -1.0;
    private double mMaxOutput = 1.0;
    private double mAllowableError;
    private double mSetPoint;
    private double mVelocitySetPoint;



    public REVSparkMaxSpeedController(CANSparkMax speedController) {
        mSpeedController = speedController;   
        mPid = mSpeedController.getPIDController(); 
    }

    @Override
    public void set(double speed) {        
        mSpeedController.set(speed);
    }

    @Override
    public double get() {      
        return mSpeedController.get();
    }

    @Override
    public void setInverted(boolean isInverted) {        
       mSpeedController.setInverted(isInverted);
    }

    @Override
    public boolean getInverted() {     
        return mSpeedController.getInverted();
    }

    @Override
    public void disable() { 
        mSpeedController.disable();
    }

    @Override
    public void stopMotor() {        
        mSpeedController.stopMotor();
    }

//    @Override
//    public void pidWrite(double output) {
//        mSpeedController.pidWrite(output);
//    }

    @Override
    public void setmotionMagicSetpoint(double setPoint) {
        mSetPoint = setPoint;
        mPid.setReference(mSetPoint, ControlType.kSmartMotion);      
    }

    @Override
    public void setVelocitySetpoint(double setPoint) {
        mVelocitySetPoint = setPoint;
        mPid.setReference(setPoint, ControlType.kVelocity);
    }

    @Override
    public void setP(double p) {
        mPid.setP(p);
        mSpeedController.burnFlash();

    }

    @Override
    public void setI(double i) {
        mPid.setI(i);
        mSpeedController.burnFlash();

    }

    @Override
    public void setD(double d) {
        mPid.setD(d);
        mSpeedController.burnFlash();

    }

    @Override
    public void setIz(double Iz) {
        mPid.setIZone(Iz);
        mSpeedController.burnFlash();

    }

    @Override
    public void setFF(double ff) {
        mPid.setFF(ff);
        mSpeedController.burnFlash();

    }

    @Override
    public void setMaxPIDOutput(double maxOutput) {
        mMaxOutput = maxOutput;
        mPid.setOutputRange(mMinOutput, mMaxOutput);

    }

    @Override
    public void setMinPIDOutput(double minOutput) {
        mMinOutput = minOutput;
        mPid.setOutputRange(mMinOutput, mMaxOutput);

    }

    @Override
    public void setMaxVelocity(double maxVelocity) {
        mPid.setSmartMotionMaxVelocity(maxVelocity, 0);

    }

    @Override
    public void setMinVelocity(double minVelocity) {
        mPid.setSmartMotionMinOutputVelocity(minVelocity, 0);

    }

    @Override
    public void setMaxAcceleration(double maxAcceleration) {
        mPid.setSmartMotionMaxAccel(maxAcceleration, 0);

    }

    @Override
    public void setAllowedClosedLoopError(double allowedClosedLoopError) {
        mAllowableError = allowedClosedLoopError;
        mPid.setSmartMotionAllowedClosedLoopError(mAllowableError, 0);

    }

    @Override
    public boolean getAtSetpoint() {
        return (mSpeedController.getEncoder().getPosition() - mSetPoint) <= mAllowableError;
    }

    
    /**
     * 
     * @param enter value between 0 and 1
     * @return whether its within threshold
     */
    @Override
    public boolean getAtVelocitySetpoint(double percentError) {
        return Math.abs((Math.abs(mSpeedController.getEncoder().getVelocity() - mVelocitySetPoint)) / mVelocitySetPoint)  <= percentError;
    }


}