package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import frc.robot.subsystems.drive.TankDriveBase;
import frc.robot.utils.controllers.XSpeedController;
import frc.robot.utils.sensors.XEncoder;

/**
 * This class contains code for the 2019 Infinite Recharge Drive base. It extends our basic TankDriveBase.
 */
public class InfiniteRechargeDriveBase extends TankDriveBase {

    public InfiniteRechargeDriveBase(XSpeedController leftMotor, XSpeedController rightMotor, XEncoder leftEncoder, XEncoder rightEncoder,
        DoubleSolenoid gearShifter) {
        super(leftMotor, rightMotor, leftEncoder, rightEncoder, gearShifter);
        
    }

    public void periodic() {
        super.periodic();       
    }

}
