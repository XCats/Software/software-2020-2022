package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj.util.Color;
import frc.robot.Constants;
import frc.robot.utils.controllers.XSpeedController;
import frc.robot.utils.sensors.impl.ColorSensorWrapper;

public class ControlPanelSubsystem extends XPneumaticSubsystem {

    private final XSpeedController mControlPanelSpeedController;
    private final ColorSensorWrapper mColorSensor;

    public ControlPanelSubsystem(XSpeedController controlPanelSpeedController, ColorSensorWrapper colorSensor,
        DoubleSolenoid controlPanelArm) {
        super("Control Panel", controlPanelArm);
        mControlPanelSpeedController = controlPanelSpeedController;
        mColorSensor = colorSensor;
    }

    @Override
	public void initSendable(SendableBuilder builder) {
        super.initSendable(builder);
        builder.addDoubleProperty("CP - Spinner Speed", ControlPanelSubsystem.this::getSpeed, null);
        // builder.addDoubleArrayProperty("CP - Add Color to Match", null, ControlPanelSubsystem.this::addColorToMatch);
        // builder.addDoubleProperty("CP - Color Sensor Confidence", null, ControlPanelSubsystem.this::setColorMatchConfidenceLevel);
    }
    
    public void resetColorValues() {
        mColorSensor.reset();
    }

    public void addColorToMatch(double[] colorValueArray) {
        mColorSensor.addColorToMatch(new Color(colorValueArray[0], colorValueArray[1], colorValueArray[2]));
    }

    public void setColorMatchConfidenceLevel(double confidenceLevel) {
        mColorSensor.setConfidenceLevel(confidenceLevel);
    }

    public boolean isMatch() {
        return mColorSensor.isMatch();
    }

    public double getSpeed(){
        return mControlPanelSpeedController.get();
    }

    public void setSpeed(double speed) {
        //TODO: Use PID to ramp up to given speed
        mControlPanelSpeedController.set(speed);
    }
    
    public void spin() {
        setSpeed(.20);
    }

    public void matching() {
        setSpeed(.10);
    }

    public void stopMotor() {
        //TODO: Use PID to stop motor gradually
        mControlPanelSpeedController.set(Constants.MOTOR_STOP_SPEED.getValue());
    }

	public Color getClosestMatchedColor() {
		return mColorSensor.getMatchedDetectedColor().color;
    }
    
    public void stopPanel() {
        extend();
        stopMotor();
    }
}