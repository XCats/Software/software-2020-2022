/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.controllers.XSpeedController;
import frc.robot.utils.sensors.XEncoder;

public class TurretSubsystem extends SubsystemBase {
  private final XSpeedController mTurretController;
  private final XEncoder mEncoder;


  public TurretSubsystem(XSpeedController turretSpeedController, XEncoder encoder) {
    this.mTurretController = turretSpeedController;
    this.mTurretController.setInverted(true);
    this.mEncoder = encoder;
  }

  public void setSpeed(double speed) {
    mTurretController.set(speed);
  }

  public void bangBang(double tx) {
    if (tx < -Constants.TURRET_ALLOWABLE_ERROR.getValue()) {
      setSpeed(.2);
    } else if (tx > Constants.TURRET_ALLOWABLE_ERROR.getValue()) {
      setSpeed(-.2);
    }
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
  
  public double getEncoderDistTicks() {
    return mEncoder.getRawDistanceTicks();
  }
  
  public double getEncoderRotDist() {
    return mEncoder.getRotationalDistance();
  }
}
