package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class XPneumaticSubsystem extends SubsystemBase {

    private DoubleSolenoid mSolenoid;
    private String mName;
    private final DoubleSolenoid.Value mExtendedValue;
    private final DoubleSolenoid.Value mRetractedValue;

    public XPneumaticSubsystem(String name, DoubleSolenoid solenoid) {
        this(name, solenoid, false);
    }

    public XPneumaticSubsystem(String name, DoubleSolenoid solenoid, boolean reversed) {
        super();
        this.mSolenoid = solenoid;
		this.mName = name;
		if (reversed) {
			this.mExtendedValue = DoubleSolenoid.Value.kReverse;
			this.mRetractedValue = DoubleSolenoid.Value.kForward;
		} else {
			this.mExtendedValue = DoubleSolenoid.Value.kForward;
			this.mRetractedValue = DoubleSolenoid.Value.kReverse;
		}
    }
    
    @Override
	public void initSendable(SendableBuilder builder) {
		builder.addBooleanProperty(getName(), XPneumaticSubsystem.this::getState, null);
	}

	public void extend() {
		this.mSolenoid.set(mExtendedValue);
	}

	public void retract() {
		this.mSolenoid.set(mRetractedValue);
	}

    public void toggle() {
        if (getState() == true) {
            retract();
        } else {
            extend();
        }
    }
	public void setExtended(boolean extended) {
		if (extended) {
			this.extend();
		} else {
			this.retract();
		}
	}

	public boolean getState() {
		return mSolenoid.get() == mExtendedValue;
	}

	public String getName() {
		return this.mName;
	}

	public void initialize() {
		mSolenoid.set(mRetractedValue);
    }
    
	public void stop() {
		this.mSolenoid.set(DoubleSolenoid.Value.kOff);
	}


}
