/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import java.util.concurrent.atomic.AtomicInteger;

import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.utils.controllers.XSpeedController;
import frc.robot.utils.sensors.impl.XCounter;

public class AcquisitionRollersSubsystem extends SubsystemBase {

  private final XSpeedController msideToSideController;
  private final XSpeedController mfrontAndBackController;
  private final AtomicInteger mNumBalls;
  private XCounter mIntakeBallSensor;
  private XCounter mBallInCarouselSensor;

  /**
   * Creates a new Aquisition Subsystem.
   */
  public AcquisitionRollersSubsystem(XSpeedController sideToSideController, XSpeedController frontAndBackController,
      AtomicInteger numBalls, XCounter intakeBallSensor, XCounter ballInCarouselSensor) {
    msideToSideController = sideToSideController;
    mfrontAndBackController = frontAndBackController;
    mNumBalls = numBalls;
    mIntakeBallSensor = intakeBallSensor;
    mBallInCarouselSensor = ballInCarouselSensor;
  }

  @Override
	public void initSendable(SendableBuilder builder) {
    builder.addDoubleProperty("IntakeBallCount", AcquisitionRollersSubsystem.this::getNumBalls, null);
	}

  public void intake() {
    //System.out.println("intake!!!!!!!!!!!!!!!!!!!");
    if (mBallInCarouselSensor.seesObject() && mIntakeBallSensor.seesObject()) { // pause intake until there's a clear spot in carousel
      System.out.println("Ball sensed in carousel in front of ACQ intake");
      msideToSideController.stopMotor();
      mfrontAndBackController.stopMotor();
    } else {
      msideToSideController.set(-Constants.AQUISITION_SIDE_TO_SIDE_SPEED);
      mfrontAndBackController.set(Constants.AQUISITION_FRONT_TO_BACK_SPEED);
    }
  }

  public void outTake() {
    msideToSideController.set(Constants.AQUISITION_FRONT_TO_BACK_SPEED);
    mfrontAndBackController.set(-Constants.AQUISITION_FRONT_TO_BACK_SPEED);
  }

  public void stop() {
    msideToSideController.stopMotor();
    mfrontAndBackController.stopMotor();
  }
  
  public int getNumBalls() {
    return mNumBalls.get();
  }
 
  @Override
  public void periodic() {

    //This method will be called once per scheduler run
    if(mIntakeBallSensor.seesObject() == true){
      mNumBalls.incrementAndGet();
    }
  }
}
