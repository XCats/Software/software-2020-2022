package frc.robot.subsystems;

import java.util.concurrent.atomic.AtomicInteger;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.util.sendable.SendableBuilder;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.PortMappings;
import frc.robot.utils.PIDFProperty;
import frc.robot.utils.controllers.XSpeedController;
import frc.robot.utils.sensors.XEncoder;
import frc.robot.utils.sensors.impl.XCounter;

public class ShooterSubsystem extends SubsystemBase {

  private final XSpeedController mShooter;
  private final XSpeedController mFeeder;
  private final AtomicInteger mNumPowerCells;
  private double mSpeed;
  private XEncoder mShooterEncoder;
  private final XCounter mShooterBallCounter = new XCounter(PortMappings.SHOOTER_SENSOR, "Shooter Ball Counter");
  private final PIDFProperty shooterPIDF;
  private final SimpleMotorFeedforward mFeedForward;
  private double mFfgain;

  public ShooterSubsystem(XSpeedController shooter, XSpeedController feeder, AtomicInteger numPowerCells,
      XEncoder encoder) {
    super();
    mShooterEncoder = encoder;
    mShooter = shooter;
    mFeeder = feeder;
    mNumPowerCells = numPowerCells;
    mFeedForward = new SimpleMotorFeedforward(0.12, 0.13, 0.108);
    this.shooterPIDF = new PIDFProperty.Builder().addPProperty(Constants.DEFAULT_SHOOTER_KP, mShooter.getPConsumer())
        .addIProperty(Constants.DEFAULT_SHOOTER_KI, mShooter.getIConsumer())
        .addDProperty(Constants.DEFAULT_SHOOTER_KD, mShooter.getDConsumer()).toProperty();
    // mShooter.setInverted(false);
  }

  @Override
  public void initSendable(SendableBuilder builder) {
    builder.addDoubleProperty("Shooter Speed", ShooterSubsystem.this::getSpeed, null);
    builder.addDoubleProperty("Shooter Velocity", () -> mShooterEncoder.getRawVelocityTicks(), null);
    builder.addBooleanProperty("IsAtVelocitySetPoint", ShooterSubsystem.this::isAtVelocitySetpoint, null);
    builder.addDoubleProperty("Number of Balls", ShooterSubsystem.this::getNumPowerCells, null);
    builder.addDoubleProperty("Feedforward Gain", ShooterSubsystem.this::getFFGain, null);
  }

  @Override
  public void periodic() {
    if (mShooterBallCounter.seesObject()) {
      decrementPowerCells();
    }
    this.shooterPIDF.updateValueIfChanged();
  }

  public void setShooterSpeed(double speed) {
    mSpeed = speed;
    mShooter.set(-mSpeed);
  }

  // Sets the velocity based on distance away from target
  public void setDistance(double distanceFromTarget) {
    double setPoint = 0;
    double targetVelocity = 0;
    // TODO: Math to determine the setPoint at a given distance from target
    // 5 percent error of speed max speed derived from eq.
    // not needed anymore because of new getAtVelocitySetpoint() method in
    // XSpeedController mShooter.setAllowedClosedLoopError(targetVelocity * 0.05);
    mShooter.setVelocitySetpoint(setPoint);
  }

  public double getSpeed() {
    return mShooter.get();
  }

  public boolean isAtVelocitySetpoint() {
    return mShooter.getAtVelocitySetpoint(.02);
  }

  public void stopShooter() {
    mShooter.stopMotor();
  }

  public void setRPM(double rpm) {
    setFFGain(rpm);
    mShooter.setFF(this.mFfgain);
    mShooter.setVelocitySetpoint(rpm);
  }

  public void setFFGain(double rpm) {
    this.mFfgain = 0.000197;//mFeedForward.calculate(rpm / 60, (Constants.SHOOTER_MAX_RPM.getValue() / 60))
        /// (Constants.SHOOTER_MAX_VOLTAGE.getValue() * Constants.SHOOTER_MAX_RPM.getValue());
  }

  public double getFFGain() {
    return this.mFfgain;
  }

  public void setFeederSpeed(double speed) {
    mFeeder.set(speed);
  }

  public void stopFeeder() {
    mFeeder.set(Constants.MOTOR_STOP_SPEED.getValue());
  }

  public int getNumPowerCells() {
    return mNumPowerCells.get();
  }

  public void decrementPowerCells() {
    // mNumPowerCells.decrementAndGet();
  }
}