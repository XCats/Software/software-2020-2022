package frc.robot.auto.complex_commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.auto.base_commands.AutoTurretPIDCommand;
import frc.robot.auto.base_commands.DriveDistanceCommand;
import frc.robot.auto.base_commands.AutoTurretPIDCommand.Mode;
import frc.robot.commands.shooter.ShooterCommand;
import frc.robot.processors.Limelight;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.TurretSubsystem;
import frc.robot.subsystems.drive.TankDriveBase;

public class AutoLeftMode extends SequentialCommandGroup {

    public AutoLeftMode(TankDriveBase driveBase, TurretSubsystem turret, ShooterSubsystem shooterSubSystem,
            Limelight limelight) {
        addCommands(new DriveDistanceCommand(driveBase, 12, Constants.AUTO_SPEED), new AutoTurretPIDCommand(turret, limelight, Mode.LEFT_SIDE), new ShooterCommand(shooterSubSystem, limelight));

    }
    
}