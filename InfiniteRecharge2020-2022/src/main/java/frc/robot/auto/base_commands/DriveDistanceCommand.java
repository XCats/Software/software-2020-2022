package frc.robot.auto.base_commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.drive.TankDriveBase;

public class DriveDistanceCommand extends CommandBase {

    public TankDriveBase mDriveBase;
    private boolean mIsFinished = false;
    private double mDistance;
    private double mSpeed;

    public DriveDistanceCommand(TankDriveBase driveBase, double distance, double speed) {
        super();
        this.mDriveBase = driveBase;
        super.addRequirements(driveBase);
        mDistance = distance;
        mSpeed = speed;
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        if (mDriveBase.getAvgEncoderPosition() < mDistance) {
            this.mDriveBase.setLeftSpeed(mSpeed);
            this.mDriveBase.setRightSpeed(mSpeed);
        } else {
            mIsFinished = true;
        }
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        this.mDriveBase.setLeftSpeed(0);
        this.mDriveBase.setRightSpeed(0);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return mIsFinished;
    }
}